# Misskey Tweaks
A collection of tweaks for Misskey to improve the UI. 

| ![Profile Page](images/profile-page.png) | ![Mobile Menu](images/mobile-menu.png) | ![Note](images/post.png) |
| --- | --- | --- |

**Note:** The [Stylus browser extension](https://add0n.com/stylus.html) and [Stylus preprocessor](https://stylus-lang.com/) are two different things.

## [You can easily manage tweaks for any instance using Stylus](https://userstyles.world/style/4700/misskey-tweaks)
![](images/stylus-options.png)  *wow!*

## Adding to Misskey manually:
1. Go to [the snippets folder in this repo](snippets/) and copy the contents of the `.css` (not `.styl`) tweaks you want to add
	* If you want ***everything***, copy the contents of [`everything.css`](everything.css), instead.
2. In Misskey, go to Settings > General > Scroll to bottom > Custom CSS
3. Paste into the textbox, then click the Save button below the textbox that appears.
4. Reload the page
5. Enjoy!